FROM debian:stable

ENV CAMELOT_TOOLS python python-pip python-dev python-tk ghostscript libsm6 libxext6

RUN apt-get update && apt-get -y install $CAMELOT_TOOLS && apt-get clean

RUN pip install camelot-py[cv]

